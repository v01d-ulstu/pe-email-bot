import smtplib
import email.message
import datetime

import configurator


def main():
    settings = configurator.load_settings_from_file()
    
    if datetime.date.today().weekday() not in settings.days:
        print('Not today')
        return

    message = email.message.EmailMessage()
    message.set_content(settings.message)
    message['Subject'] = settings.subject
    message['From'] = settings.login
    message['To'] = settings.to

    smtp = smtplib.SMTP_SSL(settings.ssl_host, settings.ssl_port)
    smtp.login(settings.login, settings.password)
    smtp.send_message(message)
    smtp.quit()
    print('gj m8')


if __name__ == '__main__':
    main()
