import collections
import yaml


Settings = collections.namedtuple(
    'Settings',
    'ssl_host ssl_port login password to subject message days'
)


def load_settings_from_file(file='settings.yaml'):
    with open(file, encoding='utf-8') as fin:
        return Settings(**yaml.safe_load(fin))
